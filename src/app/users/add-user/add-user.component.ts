import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from 'src/app/_services/user.service';
import { first, catchError, map, takeUntil } from 'rxjs/operators';
import { ApiResponse } from 'src/app/_models/ApiResponse';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable, Subject } from 'rxjs';
import { RankService } from 'src/app/_services/rank.service';
import { Rank } from 'src/app/_models/Rank';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { LoaderService } from 'src/app/_services/loader.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.less']
})
export class AddUserComponent implements OnInit, OnDestroy {
  userForm: FormGroup;
  submitted: boolean;
  loading: boolean;
  ranks: Rank[];
  sending: boolean;
  destroy$: Subject<void> = new Subject<void>();

  constructor(private formBuilder: FormBuilder,
              private usersService: UserService,
              private toastr: ToastrService,
              private router: Router,
              private rankService: RankService,
              private translate: TranslateService,
              private loader: LoaderService,
              private route: ActivatedRoute) { 
                this.getRanks();
              }

  ngOnInit(): void {
    this.makeForm();
  }

  get f() { return this.userForm.controls; }

  getRanks() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.ranks) {
        this.ranks = data.ranks;
      }
    });
  }

  getDefaultRank(): Rank {
    let result: Rank;
    this.ranks.forEach((rank) => {
      if (rank.isDefault) { result = rank; }
    });
    return result;
  }

  makeForm() {
    this.userForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15), Validators.pattern('^[a-zA-Z0-9]*$')]],
      email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(255), Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,9}')]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(72)]],
      rank: [this.getDefaultRank().id]
    });
    this.loading = false;
  }

  isDefault(rank: Rank): boolean {
    if (rank.isDefault === 1) {
      return true;
    } else {
      return false;
    }
  }

  onSubmit() {
    this.submitted = true;

    if (this.userForm.invalid) {
      return;
    }

    this.sending = true;
    this.usersService.addUser({
      username: this.f.username.value,
      email: this.f.email.value,
      password: this.f.password.value,
      rank: this.f.rank.value})
        .pipe(first())
        .subscribe(
          (response: ApiResponse) => {
            this.sending = false;
            this.toastr.success(this.translate.instant(response.message, {username: this.f.username.value}), this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
            this.router.navigate(['/users']);
          },  (error) => {
            this.sending = false;
            this.toastr.error(this.translate.instant(error), this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
          });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
