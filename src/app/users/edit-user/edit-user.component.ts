import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RankService } from 'src/app/_services/rank.service';
import { first, takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Rank } from 'src/app/_models/Rank';
import { User } from 'src/app/_models/User';
import { UserService } from 'src/app/_services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { ApiResponse } from 'src/app/_models/ApiResponse';
import { LoaderService } from 'src/app/_services/loader.service';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.less']
})
export class EditUserComponent implements OnInit {
  userForm: FormGroup;
  ranks: Rank[];
  user: User;
  userID: number;
  destroy$: Subject<void> = new Subject<void>();
  submitted: boolean;
  sending: boolean;

  constructor(private formBuilder: FormBuilder,
              private rankService: RankService,
              private usersService: UserService,
              private route: ActivatedRoute,
              private toastr: ToastrService,
              private router: Router,
              private loader: LoaderService,
              private translate: TranslateService) { }

  ngOnInit(): void {
    this.route.params
          .pipe(takeUntil(this.destroy$))
          .pipe(first())
          .subscribe((params) => {
            this.userID = params.id;
          })
    this.getUser();
    this.getRanks();
    this.makeForm();
  }

  get f() { return this.userForm.controls; }

  getUser() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.user) {
        this.user = data.user;
      }
    });
  }

  getRanks() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.ranks) {
        this.ranks = data.ranks;
      }
    });
  }

  makeForm() {
    this.userForm = this.formBuilder.group({
      username: [this.user.username, [Validators.required, Validators.minLength(3), Validators.maxLength(15), Validators.pattern('^[a-zA-Z0-9]*$')]],
      email: [this.user.email, [Validators.required, Validators.minLength(5), Validators.maxLength(255), Validators.pattern('[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,9}')]],
      password: ['', [Validators.minLength(8), Validators.maxLength(72)]],
      rank: [this.user.rank]
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.userForm.invalid) {
      return;
    }

    this.sending = true;
    this.usersService.updateUser(this.userID, {
      username: this.f.username.value,
      email: this.f.email.value,
      password: this.f.password.value,
      rank: this.f.rank.value})
        .pipe(first())
        .subscribe(
          (response: ApiResponse) => {
            this.sending = false;
            this.toastr.success(this.translate.instant('user.userAdded'), this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
            this.router.navigate(['/users']);
          },  (error) => {
            this.sending = false;
            this.toastr.error(this.translate.instant(error), this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
          });


  }

}
