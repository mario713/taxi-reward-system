import { Component, OnInit, OnDestroy, ɵEMPTY_ARRAY } from '@angular/core';
import { faEye, faPencilAlt, faTrash } from '@fortawesome/free-solid-svg-icons';
import { User } from 'src/app/_models/User';
import { UserService } from 'src/app/_services/user.service';
import { first, filter, map, takeUntil } from 'rxjs/operators';
import { RankService } from 'src/app/_services/rank.service';
import { Rank } from 'src/app/_models/Rank';
import { of, Subject, EMPTY, empty } from 'rxjs';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ApiResponse } from 'src/app/_models/ApiResponse';
import { ModalConfirmComponent } from 'src/app/_shared/modal-confirm/modal-confirm.component';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from 'src/app/_services/loader.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.less']
})
export class UsersListComponent implements OnInit, OnDestroy {
  users: User[];
  ranks: Rank[];
  faEye = faEye;
  faPencilAlt = faPencilAlt;
  faTrash = faTrash;
  modalRef: NgbModalRef;
  destroy$: Subject<void> = new Subject<void>();

  constructor(private userService: UserService,
              private rankService: RankService,
              private modalService: NgbModal,
              private toastr: ToastrService,
              private translate: TranslateService,
              private route: ActivatedRoute,
              private loader: LoaderService,
              private router: Router) { 
                
               }

  ngOnInit() { 
    this.loadUsers();
    this.loadRanks();
  }

  loadUsers() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.users) {
        this.users = data.users;
      }
    });
  }

  loadRanks() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.ranks) {
        this.ranks = data.ranks;
      }
    });
  }

  async updateList() {
    this.loader.startLoading();
    await this.getRanks();
    await this.getUsers();
    this.loader.finishLoading();
  }

  getUsers() {
    return new Promise((resolve, reject) => {
      this.userService.getUsers()
          .pipe(first())
          .subscribe((users) => {
            this.users = users;
            resolve();
          }, (error) => {
            if(error.name) {
              if(error.name.match('TimeoutError')) {
                this.loader.throwTimeout(this.router.url);
                resolve();
              }
            }

            this.loader.throwError(error);
            resolve();
          });
    });
  }

  getRanks() {
    return new Promise((resolve, reject) => {
      this.rankService.getRanks()
          .pipe(first())
          .subscribe((ranks) => {
            this.ranks = ranks;
            resolve();
          }, (error) => {
            if(error.name) {
              if(error.name.match('TimeoutError')) {
                this.loader.throwTimeout(this.router.url);
                resolve();
              }
            }

            this.loader.throwError(error);
            resolve();
          });
    });
  }

  getRank(id: number) {
    let result: Rank;
    this.ranks.forEach((rank) => {
      if (id === rank.id) { result = rank; }
    });
    return result;
  }

  delete(user: User) {
    this.modalRef = this.modalService.open(ModalConfirmComponent, {centered: true});
    this.modalRef.componentInstance.confirmationText = this.translate.instant('users.deleteConfirmation', {username: user.username});
    this.modalRef.componentInstance.accepted.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.onDelete(user);
    });
  }

  onDelete(user: User) {
    this.modalRef.componentInstance.loading = true;
    this.userService.deleteUser(user.id).pipe(first()).subscribe(
      (response: ApiResponse) => {
        this.toastr.success(this.translate.instant('users.deleteSuccess', { username:  user.username}), this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
        this.onDeleteFinish();
      },  (error) => {
        this.toastr.error(this.translate.instant(error, {username: user.username }), this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
      });
  }

  onDeleteFinish() {
    this.modalRef.componentInstance.loading = false;
    this.modalRef.componentInstance.close();
    this.updateList();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
