import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { UserLog } from 'src/app/_models/UserLog';
import { UserLogsService } from 'src/app/_services/user-logs.service';

enum LogType {
  User,
  Driver,
  Client
}

@Component({
  selector: 'app-user-logs',
  templateUrl: './user-logs.component.html',
  styleUrls: ['./user-logs.component.less']
  
})

export class UserLogsComponent implements OnInit, OnDestroy {

  destroy$: Subject<void> = new Subject<void>();
  userid: number;
  limit: number;
  page: number;
  @Input() type: LogType;
  userLogs: UserLog[];

  constructor(private route: ActivatedRoute,
              private userLogsService: UserLogsService) { }

  ngOnInit(): void {
    // this.route.params.pipe(takeUntil(this.destroy$)).subscribe(params => {
    //   this.userid = +params.id;
    // });
  }

  update() {
    switch(this.type) {
      case LogType.User:
        this.userLogsService.getLogs(this.userid, this.page, this.limit)
            .pipe(first())
            .subscribe((response: UserLog[]) => {
              this.userLogs = response;
            });
        break;
    }
  }
  
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
