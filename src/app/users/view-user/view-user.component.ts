import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/_models/User';
import { Rank } from 'src/app/_models/Rank';
import { RankService } from 'src/app/_services/rank.service';
import { first, takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/_services/user.service';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { LoaderService } from 'src/app/_services/loader.service';

enum LogType {
  User,
  Driver,
  Client
}

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.less']
})
export class ViewUserComponent implements OnInit, OnDestroy {
  loading: boolean;
  error: boolean;
  error_msg: string;
  timeout: boolean;
  user: User;
  ranks: Rank[];
  userId: number;
  destroy$: Subject<void> = new Subject<void>();
  logType: LogType;

  constructor(private rankService: RankService,
              private toastr: ToastrService,
              private usersService: UserService,
              private route: ActivatedRoute,
              private loader: LoaderService) { 
                this.logType = LogType.User;
               }

  ngOnInit(): void {
    this.route.params.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.userId = +params.id;
    });
    this.getUser();
    this.getRanks();
  }

  getUser() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.user) {
        this.user = data.user;
      }
    });
  }

  getRanks() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.ranks) {
        this.ranks = data.ranks;
      }
    });
  }

  getRank(id: number) {
    let result: Rank;
    this.ranks.forEach((rank) => {
      if (id === rank.id) { result = rank; }
    });
    return result;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
