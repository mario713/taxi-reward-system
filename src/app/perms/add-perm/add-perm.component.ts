import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { RankService } from 'src/app/_services/rank.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApiResponse } from 'src/app/_models/ApiResponse';
import { LoaderService } from 'src/app/_services/loader.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-perm',
  templateUrl: './add-perm.component.html',
  styleUrls: ['./add-perm.component.less']
})
export class AddPermComponent implements OnInit {
  permForm: FormGroup;
  defaultColor = '#ff0000';
  submitted: boolean;
  sending: boolean;
  error: string;


  constructor(private formBuilder: FormBuilder,
              private rankService: RankService,
              private router: Router,
              private toastr: ToastrService,
              private loader: LoaderService,
              private translate: TranslateService) { }

  ngOnInit() {
    this.permForm = this.formBuilder.group({
      permName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(15), Validators.pattern('^[a-zA-Z]*$')]],
      color: [this.defaultColor],
      perms: ['user']
    });
  }

  get f() { return this.permForm.controls; }

  onColorChange(event: Event) {
    this.f.color.setValue(event);
  }

  onSubmit() {
    this.submitted = true;

    if (this.permForm.invalid) {
      return;
    }

    this.sending = true;
    this.rankService.addRank({id: undefined, name: this.f.permName.value, color: this.f.color.value, perms: this.f.perms.value})
        .pipe(first())
        .subscribe(
          (response: ApiResponse) => {
            this.sending = false;
            this.toastr.success(this.translate.instant(response.message), this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
            this.router.navigate(['/perms'])
          },  (error) => {
            this.toastr.error(this.translate.instant(error), this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
            this.sending = false;
          });
  }
}
