import { Component, OnInit } from '@angular/core';
import { faPencilAlt, faTrash, faToggleOn } from '@fortawesome/free-solid-svg-icons';
import { RankService } from 'src/app/_services/rank.service';
import { Rank } from 'src/app/_models/Rank';
import { first, takeUntil } from 'rxjs/operators';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { ModalConfirmComponent } from 'src/app/_shared/modal-confirm/modal-confirm.component';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { LoaderService } from 'src/app/_services/loader.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-perms-list',
  templateUrl: './perms-list.component.html',
  styleUrls: ['./perms-list.component.less']
})
export class PermsListComponent implements OnInit {

  faPencil = faPencilAlt;
  faTrash = faTrash;
  faToggleOn = faToggleOn;
  ranks: Rank[];
  modalRef: NgbModalRef;
  private destroy$: Subject<void> = new Subject<void>();

  constructor(private rankService: RankService,
              private modalService: NgbModal,
              private toastr: ToastrService,
              private translate: TranslateService,
              private loader: LoaderService,
              private route: ActivatedRoute,
              private router: Router) { 
                this.loadRanks();
               }

  ngOnInit() { }

  loadRanks() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.ranks) {
        this.ranks = data.ranks;
      }
    });
  }

  updateList() {
    this.loader.startLoading();
    this.rankService.getRanks()
        .pipe(first())
        .subscribe((ranks) => {
          this.ranks = ranks;
          this.loader.finishLoading();
        }, (error) => {
          if(error.name) {
            if(error.name.match('TimeoutError')) {
              this.loader.throwTimeout(this.router.url);
              return;
            }
          }

          this.loader.throwError(error);
        });
  }

  delete(rank: Rank) {
    this.modalRef = this.modalService.open(ModalConfirmComponent, {centered: true});
    this.modalRef.componentInstance.confirmationText = this.translate.instant('permissions.deleteConfirmationText', {rankName: rank.name});
    this.modalRef.componentInstance.accepted.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.onDelete(rank);
    });
  }

  onDelete(rank: Rank) {
    this.modalRef.componentInstance.loading = true;
    this.rankService.deleteRank(rank.id).pipe(first()).subscribe((response) => {
      this.toastr.success(this.translate.instant('permissions.deleteSuccess', {rankName: rank.name}), this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
      this.onDeleteFinish();
    },  (error) => {
      this.toastr.error(this.translate.instant(error, {rankName: rank.name}), this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
      this.modalRef.componentInstance.loading = false;
      this.modalRef.componentInstance.close();
    });
  }

  onDeleteFinish() {
    this.modalRef.componentInstance.loading = false;
    this.modalRef.componentInstance.close();
    this.updateList();
  }

}
