import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil, first } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Rank } from 'src/app/_models/Rank';
import { RankService } from 'src/app/_services/rank.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { LoaderService } from 'src/app/_services/loader.service';

@Component({
  selector: 'app-edit-perm',
  templateUrl: './edit-perm.component.html',
  styleUrls: ['./edit-perm.component.less']
})
export class EditPermComponent implements OnInit, OnDestroy {
  permForm: FormGroup;
  defaultColor = '1b3e8d';
  rank: Rank;
  rankID: number;
  updating: boolean;
  submitted: boolean;
  destroy$: Subject<void> = new Subject<void>();


  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private rankService: RankService,
              private router: Router,
              private toastr: ToastrService,
              private translate: TranslateService,
              private loader: LoaderService) { 
                this.loadRank();
               }

  get f() { return this.permForm.controls; }

  ngOnInit(): void {
    this.route.params
          .pipe(takeUntil(this.destroy$))
          .pipe(first())
          .subscribe((params) => {
            this.rankID = params.id;
          })
    this.makeForm();
    
  }

  loadRank() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.rank) {
        this.rank = data.rank;
      }
    });
  }

  onColorChange(event: Event) {
    this.f.color.setValue(event);
  }

  makeForm() {
    this.permForm = this.formBuilder.group({
      permName: [this.rank.name, [Validators.required, Validators.minLength(3), Validators.maxLength(15), Validators.pattern('^[a-zA-Z]*$')]],
      color: ['#'+this.rank.color],
      perms: [this.rank.perms]
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.permForm.invalid) {
      return;
    }

    this.updating = true;
    this.rankService.updateRank({ id: this.rankID, name: this.f.permName.value, color: this.f.color.value, perms: this.f.perms.value })
          .pipe(first())
          .subscribe(
            (response: any) => {
              this.toastr.success(this.translate.instant(response.message, {rankID: response.rankID}), this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
              this.updating = false;
              this.router.navigate(['/perms']);
            },  (error) => {
              this.toastr.error(JSON.stringify(error), this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
              this.updating = false;
            }
          );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
