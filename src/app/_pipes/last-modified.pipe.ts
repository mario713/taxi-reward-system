import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'lastModified'
})
export class LastModifiedPipe implements PipeTransform {

  constructor(private datePipe: DatePipe) {}

  transform(value: string): string {
    if (Number(value) === 0 || Number(value) === null) {
      return 'Nie';
    } else {
      return this.datePipe.transform(value, 'dd-MM-y H:mm');
    }
  }

}
