import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'detectData'
})
export class DetectDataPipe implements PipeTransform {

  constructor(private translate: TranslateService) { }

  transform(value: string): string {
    if (value === 'undefined' || !value) {
      return this.translate.instant('pipes.none');
    } else {
      return value;
    }
  }

}
