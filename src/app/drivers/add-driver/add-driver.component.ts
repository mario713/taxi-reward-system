import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DriverService } from 'src/app/_services/driver.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Driver } from 'src/app/_models/Driver';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-driver',
  templateUrl: './add-driver.component.html',
  styleUrls: ['./add-driver.component.less']
})
export class AddDriverComponent implements OnInit {
  private driverForm: FormGroup;
  private sending: boolean;
  private submitted: boolean;
  constructor(private formBuilder: FormBuilder,
              private driverService: DriverService,
              private router: Router,
              private toastr: ToastrService,
              private translate: TranslateService) { }

  ngOnInit() {
    this.makeForm();
  }

  get f() { return this.driverForm.controls; }

  makeForm() {
    this.driverForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(30), Validators.pattern('^[a-zA-Z]*$')]],
      lastName: ['', [Validators.required, Validators.maxLength(30), Validators.pattern('^[a-zA-Z]*$')]],
      number: ['', [Validators.required, Validators.maxLength(10), Validators.pattern('^[0-9]*$')]],
      phoneNumber: ['', [Validators.maxLength(15), Validators.pattern('^[0-9]*$')]]
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.driverForm.invalid) {
      return;
    }

    this.sending = true;
    this.driverService.addDriver(this.f.name.value, this.f.lastName.value, this.f.number.value, this.f.phoneNumber.value)
        .pipe(first())
        .subscribe(
          (response: Driver) => {
            this.sending = false;
            this.toastr.success(this.translate.instant('driver.driverAdded'), this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
            this.router.navigate(['/drivers']);
          },
          (error) => {
            this.toastr.error(error, this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
            this.sending = false;
          });
  }
}
