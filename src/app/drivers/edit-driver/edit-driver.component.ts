import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, first } from 'rxjs/operators';
import { DriverService } from 'src/app/_services/driver.service';
import { Driver } from 'src/app/_models/Driver';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from 'src/app/_services/loader.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-driver',
  templateUrl: './edit-driver.component.html',
  styleUrls: ['./edit-driver.component.less']
})
export class EditDriverComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();
  private driverID: number;
  private driver: Driver;
  private sending: boolean;
  private driverForm: FormGroup;
  private submitted: boolean;

  constructor(private route: ActivatedRoute,
              private driverService: DriverService,
              private formBuilder: FormBuilder,
              private router: Router,
              private toastr: ToastrService,
              private loader: LoaderService,
              private translate: TranslateService) { }

  ngOnInit() {
    this.route.params.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.driverID = +params.id;
    });
    this.getDriver();
    this.makeForm();
  }

  private get f() { return this.driverForm.controls; }

  getDriver() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.driver) {
        this.driver = data.driver;
      }
    });
  }

  makeForm() {
    this.driverForm = this.formBuilder.group({
          name: [this.driver.name, [Validators.required, Validators.maxLength(30), Validators.pattern('^[a-zA-Z]*$')]],
          lastName: [this.driver.lastName, [Validators.maxLength(30), Validators.pattern('^[a-zA-Z]*$')]],
          number: [this.driver.number, [Validators.required, Validators.maxLength(10), Validators.pattern('^[0-9]*$')]],
          phoneNumber: [this.driver.phoneNumber, [Validators.maxLength(15), Validators.pattern('^[0-9+.]*$')]]
        });
  }

  private onSubmit() {
    this.submitted = true;

    if (this.driverForm.invalid) {
      return;
    }

    this.sending = true;
    this.driverService.updateDriver(this.driverID, this.f.name.value, this.f.lastName.value, this.f.number.value, this.f.phoneNumber.value)
        .pipe(first())
        .subscribe(
          (response) => {
            this.sending = false;
            this.toastr.success(this.translate.instant('driver.driverAdded'), this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
            this.router.navigate(['/drivers']);
        },
          (error) => {
            this.sending = false;
            this.toastr.error(JSON.stringify(error), this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
        });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
