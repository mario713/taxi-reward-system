import { Component, OnInit, Input } from '@angular/core';
import { Driver } from 'src/app/_models/Driver';

@Component({
  selector: 'app-driver-logs',
  templateUrl: './driver-logs.component.html',
  styleUrls: ['./driver-logs.component.less']
})
export class DriverLogsComponent implements OnInit {

  @Input() driver: Driver;
  @Input() loading: boolean;

  constructor() { }

  ngOnInit() {
  }

}
