import { Component, OnInit, OnDestroy } from '@angular/core';
import { Driver } from 'src/app/_models/Driver';
import { DriverService } from 'src/app/_services/driver.service';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil, first } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { LoaderService } from 'src/app/_services/loader.service';

enum LogType {
  User,
  Driver,
  Client
}

@Component({
  selector: 'app-view-driver',
  templateUrl: './view-driver.component.html',
  styleUrls: ['./view-driver.component.less']
})
export class ViewDriverComponent implements OnInit, OnDestroy {
  private driver: Driver;
  private driverID: number;
  private destroy$ = new Subject<void>();

  constructor(private route: ActivatedRoute,
              private driverSerivce: DriverService,
              private loader: LoaderService) { }

  ngOnInit() {
    this.route.params.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.driverID = +params.id;
    });

    this.getDriver();
  }

  getDriver() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.driver) {
        this.driver = data.driver;
      }
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
