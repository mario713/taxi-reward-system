import { Component, OnInit, OnDestroy } from '@angular/core';
import { Driver } from '../../_models/Driver';
import { DriverService } from '../../_services/driver.service';
import { Subject } from 'rxjs';
import { takeUntil, first } from 'rxjs/operators';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { faPencilAlt, faTrash, faEye } from '@fortawesome/free-solid-svg-icons';
import { ModalConfirmComponent } from 'src/app/_shared/modal-confirm/modal-confirm.component';
import { ToastrService } from 'ngx-toastr';
import { ApiResponse } from 'src/app/_models/ApiResponse';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from 'src/app/_services/loader.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-drivers-list',
  templateUrl: './drivers-list.component.html',
  styleUrls: ['./drivers-list.component.less']
})
export class DriversListComponent implements OnInit, OnDestroy {
  drivers: Driver[] = [];
  private destroy$: Subject<void> = new Subject<void>();
  modalRef: NgbModalRef;

  // Icons
  faPencil = faPencilAlt;
  faTrash = faTrash;
  faEye = faEye;

  constructor(private driverService: DriverService,
              private modalService: NgbModal,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private loader: LoaderService,
              private router: Router,
              private translate: TranslateService) { }

  ngOnInit() {
    this.loadDrivers();
  }

  loadDrivers() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.drivers) {
        this.drivers = data.drivers;
      }
    });
  }

  private updateList() {
    this.loader.startLoading();
    this.driverService.getDrivers().subscribe((drivers) => {
      this.drivers = drivers;
      this.loader.finishLoading();
    }, (error) => {
      if(error.name) {
        if(error.name.match('TimeoutError')) {
          this.loader.throwTimeout(this.router.url);
          return;
        }
      }
      this.loader.throwError(error);
    });
  }


  private delete(driver: Driver) {
    this.modalRef = this.modalService.open(ModalConfirmComponent, {centered: true});
    this.modalRef.componentInstance.confirmationText = this.translate.instant('driver.deleteConfirmation', {driverName: driver.name, driverLastName: driver.lastName});
    this.modalRef.componentInstance.accepted.pipe(takeUntil(this.destroy$)).subscribe(() => {
      this.onDelete(driver);
    });
  }

  private onDelete(driver: Driver) {
    this.modalRef.componentInstance.loading = true;
    this.driverService.deleteDriver(driver.id)
          .pipe(first())
          .subscribe((response: ApiResponse) => {
            this.toastr.success(this.translate.instant('driver.deleteSuccess'), this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
            this.onDeleteFinish();
          },  (error) => {
            this.toastr.error(this.translate.instant(error, {driverName: driver.name, driverLastName: driver.lastName}), this.translate.instant('general.failure'), { timeOut: environment.toastrTimeout });
          });
  }

  private onDeleteFinish() {
    this.modalRef.componentInstance.loading = false;
    this.modalRef.componentInstance.close();
    this.updateList();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
