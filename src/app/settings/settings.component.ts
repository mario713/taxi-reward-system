import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SettingsService } from '../_services/settings.service';
import { Settings } from '../_models/Settings';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { ApiResponse } from '../_models/ApiResponse';
import { ActivatedRoute } from '@angular/router';
import { LoaderService } from '../_services/loader.service';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.less']
})
export class SettingsComponent implements OnInit {
  settingsForm: FormGroup;
  settings: Settings;
  submitted: boolean;
  sending: boolean;

  constructor(private formBuilder: FormBuilder,
              private settingsService: SettingsService,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private loader: LoaderService,
              private translate: TranslateService) { 
                this.getSettings();
               }

  ngOnInit(): void {
    this.makeForm();
  }

  get f() { return this.settingsForm.controls; }

  getSettings() {
    this.route.data.subscribe((data) => {
      if(data.settings) {
        this.settings = data.settings;
      }
    });
  }

  makeForm() {
    this.settingsForm = this.formBuilder.group({
      title: [this.settings.title],
      language: [this.settings.language]
    });
  }

  onSubmit() {
    this.submitted = true;

    if(this.settingsForm.invalid) {
      return;
    }

    this.sending = true;
    this.settingsService.updateSettings({title: this.f.title.value, language: this.f.language.value})
          .pipe(first())
          .subscribe(
            (response: ApiResponse) => {
              this.sending = false;
              this.toastr.success(this.translate.instant(response.message), this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
            }, (error) => {
              this.sending = false;
              if(error.name) {
                if(error.name.match('TimeoutError')) {
                  this.toastr.error(this.translate.instant('error.queryTimeout'), this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
                  return;
                }
              }
              this.toastr.error(this.translate.instant(error), this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
            }
          );
  }

}
