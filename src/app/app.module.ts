import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { HeaderComponent } from './layout/header/header.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HelpComponent } from './help/help.component';
import { CoursesListComponent } from './courses/courses-list/courses-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthModule } from './auth/auth.module';
import { PermsListComponent } from './perms/perms-list/perms-list.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AddPermComponent } from './perms/add-perm/add-perm.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ColorPickerModule } from 'ngx-color-picker';
import { EditPermComponent } from './perms/edit-perm/edit-perm.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { UsersListComponent } from './users/users-list/users-list.component';
import { AddUserComponent } from './users/add-user/add-user.component';
import { EditUserComponent } from './users/edit-user/edit-user.component';
import { ViewUserComponent } from './users/view-user/view-user.component';
import { UserLogsComponent } from './users/view-user/user-logs/user-logs.component';
import { AddCourseComponent } from './courses/add-course/add-course.component';
import { GratisTableComponent } from './courses/courses-list/gratis-table/gratis-table.component';
import { CoursesTableComponent } from './courses/courses-list/courses-table/courses-table.component';
import { CoursesPaginationComponent } from './courses/courses-list/courses-table/courses-pagination/courses-pagination.component';
import { GratisPaginationComponent } from './courses/courses-list/gratis-table/gratis-pagination/gratis-pagination.component';
import { DetectDataPipe } from './_pipes/detect-data.pipe';
import { EditCourseComponent } from './courses/edit-course/edit-course.component';
import { CommonModule, DatePipe } from '@angular/common';
import { ViewClientComponent } from './clients/view-client/view-client.component';
import { ClientsListComponent } from './clients/clients-list/clients-list.component';
import { ClientsPaginationComponent } from './clients/clients-list/clients-pagination/clients-pagination.component';
import { LastModifiedPipe } from './_pipes/last-modified.pipe';
import { ClientLogsComponent } from './clients/view-client/client-logs/client-logs.component';
import { EditClientComponent } from './clients/edit-client/edit-client.component';
import { ModalConfirmComponent } from './_shared/modal-confirm/modal-confirm.component';
import { SettingsComponent } from './settings/settings.component';
import { AppconfigModule } from './_modules/appconfig.module';
import { I18nModule } from './_modules/i18n.module';
import { ServerErrorComponent } from './server-error/server-error.component';
import { LoaderComponent } from './layout/loader/loader.component';
import { DriversListComponent } from './drivers/drivers-list/drivers-list.component';
import { AddDriverComponent } from './drivers/add-driver/add-driver.component';
import { EditDriverComponent } from './drivers/edit-driver/edit-driver.component';
import { ViewDriverComponent } from './drivers/view-driver/view-driver.component';
import { DriverLogsComponent } from './drivers/view-driver/driver-logs/driver-logs.component';
import { LogsComponent } from './logs/logs.component';
import { ProfileLogsComponent } from './_shared/profile-logs/profile-logs.component';
import { PaginationComponent } from './_shared/pagination/pagination.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HeaderComponent,
    SidebarComponent,
    DashboardComponent,
    HelpComponent,
    CoursesListComponent,
    PermsListComponent,
    NotFoundComponent,
    AddPermComponent,
    EditPermComponent,
    UsersListComponent,
    AddUserComponent,
    EditUserComponent,
    ViewUserComponent,
    UserLogsComponent,
    AddCourseComponent,
    GratisTableComponent,
    GratisPaginationComponent,
    CoursesTableComponent,
    CoursesPaginationComponent,
    DetectDataPipe,
    EditCourseComponent,
    ViewClientComponent,
    ClientsListComponent,
    ClientsPaginationComponent,
    LastModifiedPipe,
    ClientLogsComponent,
    EditClientComponent,
    ModalConfirmComponent,
    SettingsComponent,
    ServerErrorComponent,
    LoaderComponent,
    DriversListComponent,
    AddDriverComponent,
    EditDriverComponent,
    ViewDriverComponent,
    DriverLogsComponent,
    LogsComponent,
    ProfileLogsComponent,
    PaginationComponent
  ],
  imports: [
    AppconfigModule,
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    NgbModule,
    AuthModule,
    ReactiveFormsModule,
    FormsModule,
    ColorPickerModule,
    BrowserAnimationsModule,
    CommonModule,
    ToastrModule.forRoot(),
    I18nModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
