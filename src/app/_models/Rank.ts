export class Rank {
    id: number;
    name: string;
    color: string;
    perms: string;
    isDefault?: number;
}
