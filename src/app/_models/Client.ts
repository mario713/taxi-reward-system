export class Client {
    id?: number;
    number?: number;
    gratis: number;
    name?: string;
    lastName?: string;
    phoneNumber?: string;
}