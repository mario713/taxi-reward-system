export class Driver {
    id: number;
    name: string;
    lastName: string;
    number: number;
    phoneNumber: string;
}