export class User {
    id?: number;
    username: string;
    email: string;
    password?: string;
    rank: number;
    passReminderToken?: string;
    token?: string;
}
