export class ClientLog {
    id: number;
    userid: number;
    time: number;
    type: string;
    log: string;
}