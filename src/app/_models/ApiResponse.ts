export class ApiResponse {
    code: number;
    message: string;
}