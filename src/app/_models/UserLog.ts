export class UserLog {
    id: number;
    userid: number;
    time: number;
    type: string;
    log: string;
}