export class DriverLog {
    id: number;
    userid: number;
    time: number;
    type: string;
    log: string;
}