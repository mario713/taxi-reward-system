import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { DriverService } from 'src/app/_services/driver.service';
import { first, takeUntil } from 'rxjs/operators';
import { Driver } from 'src/app/_models/Driver';
import { ToastrService } from 'ngx-toastr';
import { CourseService } from 'src/app/_services/course.service';
import { ApiResponse } from 'src/app/_models/ApiResponse';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { LoaderService } from 'src/app/_services/loader.service';

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.less']
})
export class AddCourseComponent implements OnInit, OnDestroy {
  courseForm: FormGroup;
  drivers: Driver[];
  sending: boolean;
  submitted: boolean;
  destroy$: Subject<void> = new Subject<void>();

  constructor(private formBuilder: FormBuilder,
              private toastr: ToastrService,
              private courseService: CourseService,
              private router: Router,
              private route: ActivatedRoute,
              private translate: TranslateService,
              private loader: LoaderService) { }

  ngOnInit(): void {
    this.getData();
    this.makeForm();
  }

  get f() { return this.courseForm.controls; }

  getData() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.drivers) {
        this.drivers = data.drivers;
      }
    });
  }

  makeForm() {
    this.courseForm = this.formBuilder.group({
      driver: [this.drivers[0].id],
      client: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.courseForm.invalid) {
      return;
    }

    this.sending = true;
    this.courseService.addCourse({driver: this.f.driver.value, client: this.f.client.value})
          .pipe(first())
          .subscribe(
            (response: ApiResponse) => {
              this.sending = false;
              this.toastr.success(this.translate.instant('course.courseAdded'), this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
              this.router.navigate(['/courses']);
            },  (error) => {
              this.toastr.error(JSON.stringify(error), this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
              this.sending = false;
            }
          );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
