import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ClientService } from 'src/app/_services/client.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-gratis-pagination',
  templateUrl: './gratis-pagination.component.html',
  styleUrls: ['./gratis-pagination.component.less']
})
export class GratisPaginationComponent implements OnInit {
  currentPage: number = 1;
  pageSize: number = 10;
  pagesCount: number;
  pages = [5, 10, 25, 50];
  @Output() changed: EventEmitter<void> = new EventEmitter();
  loaded: boolean;

  constructor(private clientService: ClientService,
              private toastr: ToastrService,
              private translate: TranslateService) { }

  ngOnInit(): void {
    this.update();
  }

  update() {
    this.clientService.getGratisPagesCount(this.pageSize)
          .pipe(first())
          .subscribe(
            (response: number) => {
              this.pagesCount = Number(response);
              this.loaded = true;
            },  (error) => {
              this.toastr.error(error, this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
            }
          );
  }

  changePageSize(size: number) {
    this.pageSize = size;
    this.currentPage = 1;
    this.update();
    this.changed.emit();
  }

  changePage(page: number) {
    this.currentPage = page;
    this.changed.emit();
  }

}
