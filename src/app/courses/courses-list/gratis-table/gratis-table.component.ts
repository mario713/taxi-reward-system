import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Client } from 'src/app/_models/Client';
import { ClientService } from 'src/app/_services/client.service';
import { first, takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { faCar, faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import { GratisPaginationComponent } from './gratis-pagination/gratis-pagination.component';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiResponse } from 'src/app/_models/ApiResponse';
import { ModalConfirmComponent } from 'src/app/_shared/modal-confirm/modal-confirm.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { LoaderService } from 'src/app/_services/loader.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-gratis-table',
  templateUrl: './gratis-table.component.html',
  styleUrls: ['./gratis-table.component.less']
})
export class GratisTableComponent implements OnInit, OnDestroy {
  clients: Client[];
  loading: boolean;
  @ViewChild(GratisPaginationComponent) pagination: GratisPaginationComponent;
  modalRef: NgbModalRef;
  destroy$: Subject<void> = new Subject<void>();

  faCar = faCar;
  faPencilAlt = faPencilAlt;

  constructor(private clientService: ClientService,
              private toastr: ToastrService,
              private modalService: NgbModal,
              private route: ActivatedRoute,
              private translate: TranslateService,
              private router: Router) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.gratisClients) {
        this.clients = data.gratisClients;
      }
    });
  }

  updateData() {
    this.loading = true;
    this.clientService.getGratisClients(this.pagination.currentPage - 1, this.pagination.pageSize)
      .subscribe((response: Client[]) => {
        this.clients = response;
        this.loading = false;
      }, (error) => {
        this.toastr.error(JSON.stringify(error), this.translate.instant('general.failure'));
        this.loading = false;
      });
  }

  open(client: number) {
    this.modalRef = this.modalService.open(ModalConfirmComponent, {centered: true});
    this.modalRef.componentInstance.confirmationText = this.translate.instant('course.courseUseConfirmation', {clientNumber: client});
    this.modalRef.componentInstance.accepted
          .pipe(first())
          .subscribe(() => {
            this.useGratis(client);
          });
  }

  useGratis(client: number) {
    this.modalRef.componentInstance.loading = true;
    this.clientService.useGratisCourse(client)
          .pipe(first())
          .subscribe(
          (response: ApiResponse) => {
            this.modalRef.componentInstance.loading = false;
            this.modalRef.componentInstance.close();
            this.updateData();
            this.toastr.success(JSON.stringify(response.message), this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
          },  (error) => {
            this.modalRef.componentInstance.loading = false;
            this.toastr.error(JSON.stringify(error), this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
          }
          );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
