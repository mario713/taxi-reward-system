import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { Driver } from 'src/app/_models/Driver';
import { DriverService } from 'src/app/_services/driver.service';
import { ToastrService } from 'ngx-toastr';
import { first, takeUntil } from 'rxjs/operators';
import { CourseService } from 'src/app/_services/course.service';
import { Course } from 'src/app/_models/Course';
import { CoursesPaginationComponent } from './courses-pagination/courses-pagination.component';
import { faPencilAlt, faTrash } from '@fortawesome/free-solid-svg-icons';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalConfirmComponent } from 'src/app/_shared/modal-confirm/modal-confirm.component';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { LoaderService } from 'src/app/_services/loader.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-courses-table',
  templateUrl: './courses-table.component.html',
  styleUrls: ['./courses-table.component.less']
})
export class CoursesTableComponent implements OnInit, OnDestroy {
  loading: boolean;
  drivers: Driver[];
  courses: Course[];
  @ViewChild(CoursesPaginationComponent) pagination: CoursesPaginationComponent;
  modalRef: NgbModalRef;
  destroy$: Subject<void> = new Subject<void>();

  faPencilAlt = faPencilAlt;
  faTrash = faTrash;

  constructor(private driverService: DriverService,
              private toastr: ToastrService,
              private courseService: CourseService,
              private modalService: NgbModal,
              private route: ActivatedRoute,
              private translate: TranslateService,
              private loader: LoaderService) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.drivers) {
        this.drivers = data.drivers;
      }
      if(data.courses) {
        this.courses = data.courses;
      }
    });
  }

  updateCourses() {
    this.loading = true;
    this.courseService.getCourses(this.pagination.currentPage - 1, this.pagination.pageSize)
      .subscribe((response) => {
        this.courses = response;
        this.loading = false;
      }, (error) => {
        this.loading = false;
        this.loader.throwError(error);
      });
  }

  getDriver(id: number): Driver {
    let result: Driver;
    this.drivers.forEach((driver) => {
      if (driver.id === id) { result = driver; }
    });

    return result;
  }

  delete(course: Course) {
    this.modalRef = this.modalService.open(ModalConfirmComponent, {centered: true});
    this.modalRef.componentInstance.confirmationText = this.translate.instant('course.deleteConfirmation');
    this.modalRef.componentInstance.accepted
          .pipe(first())
          .subscribe(() => {
            this.onDelete(course.id);
          });
  }

  onDelete(id: number) {
    this.modalRef.componentInstance.loading = true;
    this.courseService.deleteCourse(id)
          .pipe(first())
          .subscribe(
            (response) => {
              this.toastr.success(this.translate.instant('course.courseDeleted'), this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
              this.modalRef.componentInstance.loading = false;
              this.modalRef.close();
              this.updateCourses();
            },  (error) => {
              this.toastr.error(this.translate.instant(JSON.stringify(error)), this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
              this.modalRef.componentInstance.loading = false;
            }
          );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
