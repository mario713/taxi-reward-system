import { Component, OnInit, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { CourseService } from 'src/app/_services/course.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-courses-pagination',
  templateUrl: './courses-pagination.component.html',
  styleUrls: ['./courses-pagination.component.less']
})
export class CoursesPaginationComponent implements OnInit {
  currentPage: number = 1;
  pageSize: number = 10;
  pagesCount: number;
  pages = [5, 10, 25, 50];
  @Output() changed: EventEmitter<void> = new EventEmitter();
  loaded: boolean;

  constructor(private courseService: CourseService,
              private toastr: ToastrService,
              private translate: TranslateService) { }

  ngOnInit() {
    this.update();
  }

  update() {
    this.courseService.getPagesCount(this.pageSize)
          .pipe(first())
          .subscribe(
            (response: number) => {
              this.pagesCount = Number(response);
              this.loaded = true;
            },  (error) => {
              this.toastr.error(error, this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
            }
          );
  }

  changePageSize(size: number) {
    this.pageSize = size;
    this.currentPage = 1;
    this.update();
    this.changed.emit();
  }

  changePage(page: number) {
    this.currentPage = page;
    this.changed.emit();
  }

}
