import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/_services/loader.service';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.less']
})
export class CoursesListComponent {
  selectedTab: number = 0;

  constructor(private loader: LoaderService) { }

  selectTab(tab: number) {
    this.selectedTab = tab;
  }

}
