import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Course } from 'src/app/_models/Course';
import { CourseService } from 'src/app/_services/course.service';
import { first, takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { DriverService } from 'src/app/_services/driver.service';
import { Driver } from 'src/app/_models/Driver';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
import { LoaderService } from 'src/app/_services/loader.service';

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.less']
})
export class EditCourseComponent implements OnInit, OnDestroy {
  courseForm: FormGroup;
  course: Course;
  drivers: Driver[];
  courseID: number;
  sending: boolean;
  destroy$: Subject<void> = new Subject<void>();
  submitted: boolean;

  constructor(private formBuilder: FormBuilder,
              private courseService: CourseService,
              private toastr: ToastrService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private route: ActivatedRoute,
              private translate: TranslateService,
              private loader: LoaderService) { }

  ngOnInit(): void {
    this.activatedRoute.params.pipe(takeUntil(this.destroy$)).subscribe(params => {
      this.courseID = +params.id;
    });
    this.getData();
    this.makeForm();
  }

  get f() { return this.courseForm.controls; }

  getData() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.course) {
        this.course = data.course;
      }
      if(data.drivers) {
        this.drivers = data.drivers;
      }
    });
  }

  makeForm() {
    this.courseForm = this.formBuilder.group({
      number: [this.course.client, [Validators.required, Validators.pattern('^[0-9]*$')]],
      driver: [this.course.driver]
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.courseForm.invalid) {
      return;
    }

    this.sending = true;
    this.courseService.updateCourse(this.courseID, {driver: this.f.driver.value, client: this.f.number.value})
          .pipe(first())
          .subscribe(
            (response) => {
              this.sending = false;
              this.toastr.success(this.translate.instant('course.courseUpdated'), this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
              this.router.navigate(['/courses']);
            },  (error) => {
              this.toastr.error(JSON.stringify(error), this.translate.instant('general.failure'), {timeOut: environment.toastrTimeout});
              this.sending = false;
            }
          );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
