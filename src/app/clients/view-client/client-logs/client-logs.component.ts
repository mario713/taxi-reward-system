import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-client-logs',
  templateUrl: './client-logs.component.html',
  styleUrls: ['./client-logs.component.less']
})
export class ClientLogsComponent implements OnInit {

  @Input() error: boolean;
  @Input() loaded: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
