import { Component, OnInit, OnDestroy } from '@angular/core';
import { Client } from 'src/app/_models/Client';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil, first } from 'rxjs/operators';
import { ClientService } from 'src/app/_services/client.service';
import { ToastrService } from 'ngx-toastr';
import { JsonPipe } from '@angular/common';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { LoaderService } from 'src/app/_services/loader.service';

@Component({
  selector: 'app-view-client',
  templateUrl: './view-client.component.html',
  styleUrls: ['./view-client.component.less']
})
export class ViewClientComponent implements OnInit, OnDestroy {
  client: Client;
  clientID: number;
  destroy$: Subject<void> = new Subject<void>();
  dataLoaded: boolean;
  loading: boolean;
  error: boolean;
  error_msg: string;
  timeout: boolean;

  constructor(private route: ActivatedRoute,
              private loader: LoaderService) { }

  ngOnInit(): void {
    this.route.params
          .pipe(takeUntil(this.destroy$))
          .subscribe((params) => {
            this.clientID = params.id;
          });
    this.getClient();
  }

  getClient() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.client) {
        this.client = data.client;
      }
    });
  }
  
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
