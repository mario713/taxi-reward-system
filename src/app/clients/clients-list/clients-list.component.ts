import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Client } from 'src/app/_models/Client';
import { ClientService } from 'src/app/_services/client.service';
import { ClientsPaginationComponent } from './clients-pagination/clients-pagination.component';
import { takeUntil } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { faEye, faPencilAlt, faCar } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';
import { LoaderService } from 'src/app/_services/loader.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.less'],
})
export class ClientsListComponent implements OnInit, OnDestroy {
  clients: Client[];
  @ViewChild(ClientsPaginationComponent) pagination: ClientsPaginationComponent;
  faEye = faEye;
  faPencilAlt = faPencilAlt;
  faCar = faCar;
  destroy$: Subject<void> = new Subject<void>();

  constructor(private clientService: ClientService,
              private toastr: ToastrService,
              private router: Router,
              private route: ActivatedRoute,
              private loader: LoaderService) { }

  ngOnInit(): void {
    this.loadClients();
  }

  loadClients() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.clients) {
        this.clients = data.clients;
      }
    });
  }

  updateClients() {
    this.loader.startLoading();
    this.clientService.getClients(this.pagination.currentPage - 1, this.pagination.pageSize)
          .subscribe((response: Client[]) => {
            this.clients = response;
            this.loader.finishLoading();
          }, (error) => {
            if(error.name) {
              if(error.name.match('TimeoutError')) {
                this.loader.throwTimeout(this.router.url);
                return;
              }
            }

            this.loader.throwError(error);
          });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
