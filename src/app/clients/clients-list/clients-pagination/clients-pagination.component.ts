import { Component, OnInit, Output, EventEmitter, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ClientService } from 'src/app/_services/client.service';
import { LoaderService } from 'src/app/_services/loader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-clients-pagination',
  templateUrl: './clients-pagination.component.html',
  styleUrls: ['./clients-pagination.component.less']
})
export class ClientsPaginationComponent implements OnInit, OnDestroy {
  currentPage: number = 1;
  pageSize: number = 10;
  pagesCount: number;
  pages = [5, 10, 25, 50];
  @Output() changed: EventEmitter<void> = new EventEmitter();
  destroy$: Subject<void> = new Subject<void>();

  constructor(private clientService: ClientService,
              private loader: LoaderService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.loadPagesCount();
  }

  loadPagesCount() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe(data => {
      if(data.pages) {
        this.pagesCount = data.pages;
        this.pageSize = 10;
      }
    });
  }

  updatePagesCount() {
    this.clientService.getClientPagesCount(this.pageSize)
      .subscribe((response: number) => {
        this.pagesCount = Number(response);
      }, (error) => {
        if(error.name) {
          if(error.name.match('TimeoutError')) {
            this.loader.throwTimeout(this.router.url);
            return;
          }
        }

        this.loader.throwError(error);
      });
  }
  
  changePageSize(size: number) {
    this.pageSize = size;
    this.currentPage = 1;
    this.updatePagesCount();
    this.changed.emit();
  }

  changePage(page: number) {
    this.currentPage = page;
    this.changed.emit();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
