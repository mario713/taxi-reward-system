import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Client } from 'src/app/_models/Client';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientService } from 'src/app/_services/client.service';
import { first, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ApiResponse } from 'src/app/_models/ApiResponse';
import { TranslateService } from '@ngx-translate/core';
import { LoaderService } from 'src/app/_services/loader.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.less']
})
export class EditClientComponent implements OnInit, OnDestroy {
  clientID: number;
  client: Client;
  clientForm: FormGroup;
  submitted: boolean;
  sending: boolean;
  destroy$: Subject<void> = new Subject<void>();

  constructor(private formBuilder: FormBuilder,
              private toastr: ToastrService,
              private route: ActivatedRoute,
              private clientService: ClientService,
              private router: Router,
              private translate: TranslateService,
              private loader: LoaderService) { }

  ngOnInit(): void {
    this.route.params
          .pipe(takeUntil(this.destroy$))
          .subscribe((params) => {
            this.clientID = params.id;
          })
    this.getClient();
    this.makeForm();
  }

  get f() { return this.clientForm.controls; }

  getClient() {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      if(data.client) {
        this.client = data.client;
      }
    });
  }

  makeForm() {
    this.clientForm = this.formBuilder.group({
      number: {value: this.client.number, disabled: true},
      gratis: [this.client.gratis, [Validators.required, Validators.min(0), Validators.maxLength(30), Validators.pattern('^[0-9]*$')]],
      name: [this.client.name, [Validators.maxLength(30), Validators.pattern('^[a-zA-Z]*$')]],
      lastName: [this.client.lastName, [Validators.maxLength(30), Validators.pattern('^[a-zA-Z]*$')]],
      phoneNumber: [this.client.phoneNumber, [Validators.maxLength(15), Validators.pattern('^[0-9+.]*$')]]
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.clientForm.invalid) {
      return;
    }

    this.sending = true;
    this.clientService.updateClient(this.clientID, {
      gratis: this.f.gratis.value,
      name: this.f.name.value,
      lastName: this.f.lastName.value,
      phoneNumber: this.f.phoneNumber.value})
            .subscribe(
              (response: ApiResponse) => {
                this.toastr.success(response.message, this.translate.instant('general.success'), {timeOut: environment.toastrTimeout});
                this.sending = false;
                this.router.navigate(['/clients']);
              },  (error) => {
                if(error.name) {
                  if(error.name.match('TimeoutError')) {
                    this.toastr.error(this.translate.instant(this.translate.instant('error.queryTimeout')), this.translate.instant('general.error'), {timeOut: environment.toastrTimeout});
                    this.sending = false;
                    return;
                  }
                }
  
                this.toastr.error(this.translate.instant(JSON.stringify(error)), this.translate.instant('general.error'), {timeOut: environment.toastrTimeout});
                this.sending = false;
              }
            );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
