import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { init_app } from './_modules/appconfig.module';
import { AppconfigService } from './_services/appconfig.service';
import { Router, NavigationStart, Event, NavigationEnd, NavigationError, NavigationCancel, ResolveEnd, RoutesRecognized, ResolveStart } from '@angular/router';
import { LoaderService } from './_services/loader.service';
import { EMPTY } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'taxi-reward-system';

  constructor(private titleService: Title,
              private appconfig: AppconfigService,
              private router: Router,
              private loader: LoaderService) { 

    this.router.events.subscribe((event: any) => {
      switch(true) {
        case event instanceof NavigationStart: {
          this.loader.startLoading();
          break;
        }

        case event instanceof NavigationEnd: {
          this.loader.finishLoading();
          break;
        }

        case event instanceof NavigationError: {
          if(event.error.name) { 
            if(event.error.name.match('TimeoutError')) {
              console.log(`MAIN: ${event.url}`);
              this.loader.throwTimeout(event.url);
              break;
            }
          }

          this.loader.throwError(event.error);
          break;
        }

        default: {
          break;
        }
      }
    });

    this.setTitle(appconfig.settings.title);
   }

  public setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }
}
