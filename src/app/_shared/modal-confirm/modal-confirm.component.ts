import { Component, OnInit, EventEmitter, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-confirm',
  templateUrl: './modal-confirm.component.html',
  styleUrls: ['./modal-confirm.component.less']
})
export class ModalConfirmComponent implements OnInit {
  loading: boolean;
  public accepted: EventEmitter<void> = new EventEmitter<void>();
  @Input() confirmationText: string;

  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  public accept() {
    this.accepted.emit();
  }

  public close() {
    this.activeModal.close();
  }
}
