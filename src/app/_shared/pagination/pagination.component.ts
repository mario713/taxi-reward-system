import { Component, OnInit, Output, EventEmitter, OnDestroy, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ClientLogsService } from 'src/app/_services/client-logs.service';
import { DriverLogsService } from 'src/app/_services/driver-logs.service';
import { LoaderService } from 'src/app/_services/loader.service';
import { UserLogsService } from 'src/app/_services/user-logs.service';

enum LogType {
  User,
  Driver,
  Client
}

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.less']
})
export class PaginationComponent implements OnInit, OnDestroy {

  destroy$: Subject<void> = new Subject<void>();
  pages = [5, 10, 25, 50];
  currentPage: number = 1;
  pageSize: number = 5;
  pagesCount: number;
  @Input() type: LogType;
  @Input() id;

  @Output() changed: EventEmitter<void> = new EventEmitter<void>();

  constructor(private userLogsService: UserLogsService,
              private driverLogsService: DriverLogsService,
              private clientLogsService: ClientLogsService,
              private loader: LoaderService,
              private router: Router) { }

  ngOnInit(): void {
    this.updatePagesCount();
    console.log(`Pagination Type: ${this.type}`);
  }

  updatePagesCount() {
    switch(+this.type) {
      case LogType.User:
        this.userLogsService.getLogsPages(this.id, this.pageSize)
        .pipe(takeUntil(this.destroy$))
        .subscribe((response: number) => {
            this.pagesCount = Number(response);
        }, (error) => {
          if(error.name) {
            if(error.name.match('TimeoutError')) {
              this.loader.throwTimeout(this.router.url);
              return;
            }
          }
  
          this.loader.throwError(error);
        });
        break;

      case LogType.Driver:
        this.driverLogsService.getLogsPages(this.id, this.pageSize)
        .pipe(takeUntil(this.destroy$))
        .subscribe((response: number) => {
            this.pagesCount = Number(response);
        }, (error) => {
          if(error.name) {
            if(error.name.match('TimeoutError')) {
              this.loader.throwTimeout(this.router.url);
              return;
            }
          }
  
          this.loader.throwError(error);
        });
        break;

      case LogType.Client:
        this.clientLogsService.getLogsPages(this.id, this.pageSize)
        .pipe(takeUntil(this.destroy$))
        .subscribe((response: number) => {
            this.pagesCount = Number(response);
        }, (error) => {
          if(error.name) {
            if(error.name.match('TimeoutError')) {
              this.loader.throwTimeout(this.router.url);
              return;
            }
          }
  
          this.loader.throwError(error);
        });
        break;
    }
  }

  changePageSize(size: number) {
    this.pageSize = size;
    this.currentPage = 1;
    this.updatePagesCount();
    this.changed.emit();
  }

  changePage(page: number) {
    this.currentPage = page;
    this.changed.emit();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
