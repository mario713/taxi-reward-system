import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { ClientLog } from 'src/app/_models/ClientLog';
import { Driver } from 'src/app/_models/Driver';
import { DriverLog } from 'src/app/_models/DriverLog';
import { UserLog } from 'src/app/_models/UserLog';
import { ClientLogsService } from 'src/app/_services/client-logs.service';
import { DriverLogsService } from 'src/app/_services/driver-logs.service';
import { UserLogsService } from 'src/app/_services/user-logs.service';
import { PaginationComponent } from '../pagination/pagination.component';

enum LogType {
  User,
  Driver,
  Client
}

@Component({
  selector: 'app-profile-logs',
  templateUrl: './profile-logs.component.html',
  styleUrls: ['./profile-logs.component.less']
})
export class ProfileLogsComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild(PaginationComponent) pagination: PaginationComponent;
  page: number = 0;
  limit: number = 5;
  @Input() type: LogType;
  logs: UserLog[];
  destroy$: Subject<void> = new Subject<void>();
  @Input() id: number;
  private loading: boolean = true;

  constructor(private userLogsService: UserLogsService,
              private driverLogsService: DriverLogsService,
              private clientLogsService: ClientLogsService) { }


  ngOnInit(): void { }
  
  ngAfterViewInit() {
    this.update();
  }

  update() {
    switch(+this.type) {
      case LogType.User:
        this.userLogsService.getLogs(this.id, this.pagination.currentPage - 1, this.pagination.pageSize)
            .pipe(takeUntil(this.destroy$))
            .pipe(first())
            .subscribe((response: UserLog[]) => {
              this.logs = response;
            });
        break;

      case LogType.Driver:
        this.driverLogsService.getLogs(this.id, this.pagination.currentPage - 1, this.pagination.pageSize)
            .pipe(takeUntil(this.destroy$))
            .pipe(first())
            .subscribe((response: DriverLog[]) => {
              this.logs = response;
            });
        break;

      case LogType.Client:
        this.clientLogsService.getLogs(this.id, this.pagination.currentPage - 1, this.pagination.pageSize)
            .pipe(takeUntil(this.destroy$))
            .pipe(first())
            .subscribe((response: ClientLog[]) => {
              this.logs = response;
              console.log(response);
              console.log(`Logs(${this.id}|${this.pagination.currentPage - 1} | ${this.pagination.pageSize}): ${this.logs}`);
            });
        break;
    }
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
