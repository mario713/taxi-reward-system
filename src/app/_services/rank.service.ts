import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Rank } from '../_models/Rank';
import { environment } from '../../environments/environment';
import { timeout, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RankService {

  constructor(private http: HttpClient) { }

  public getRanks(): Observable<Rank[]> {
    return this.http.get<Rank[]>(`${environment.apiUrl}/ranks`).pipe(timeout(environment.apiTimeout));
  }

  public getRank(id: number): Observable<Rank> {
    return this.http.get<Rank>(`${environment.apiUrl}/ranks/${id}`).pipe(timeout(environment.apiTimeout));
  }

  public addRank(rank: Rank) {
    return this.http.post(`${environment.apiUrl}/ranks`, {name: rank.name, color: rank.color, perms: rank.perms}).pipe(timeout(environment.apiTimeout));
  }

  public updateRank(rank: Rank) {
    return this.http.put(`${environment.apiUrl}/ranks/${rank.id}`, {name: rank.name, color: rank.color, perms: rank.perms}).pipe(timeout(environment.apiTimeout));
  }

  public deleteRank(id: number) {
    return this.http.delete(`${environment.apiUrl}/ranks/${id}`).pipe(timeout(environment.apiTimeout));
  }
}
