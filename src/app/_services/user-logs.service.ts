import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UserLog } from '../_models/UserLog';

@Injectable({
  providedIn: 'root'
})
export class UserLogsService {

  constructor(private http: HttpClient) { }

  getLogs(userid: number, page: number, limit: number) {
    return this.http.get<UserLog[]>(`${environment.apiUrl}/logs/user/${userid}/${page}/${limit}`)
  }

  getLogsPages(userid: number, limit: number) {
    return this.http.get<Number>(`${environment.apiUrl}/logs/userPages/${userid}/${limit}`);
  }
}
