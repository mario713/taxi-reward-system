import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ClientLog } from '../_models/ClientLog';

@Injectable({
  providedIn: 'root'
})
export class ClientLogsService {

  constructor(private http: HttpClient) { }

  getLogs(clientid: number, page: number, limit: number) {
    return this.http.get<ClientLog[]>(`${environment.apiUrl}/logs/client/${clientid}/${page}/${limit}`)
  }

  getLogsPages(clientid: number, limit: number) {
    return this.http.get<Number>(`${environment.apiUrl}/logs/clientPages/${clientid}/${limit}`);
  }
}
