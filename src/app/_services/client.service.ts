import { Injectable } from '@angular/core';
import { Client } from '../_models/Client';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }

  getGratisPagesCount(limit: number): Observable<Number> {
    return this.http.get<Number>(`${environment.apiUrl}/clients/getGratisPages/${limit}`).pipe(timeout(environment.apiTimeout));
  }

  getGratisClients(page: number, limit: number): Observable<Client[]> {
    return this.http.get<Client[]>(`${environment.apiUrl}/clients/getGratis/${page}/${limit}`).pipe(timeout(environment.apiTimeout));
  }

  useGratisCourse(clientNumber: number) {
    return this.http.get(`${environment.apiUrl}/clients/use/${clientNumber}`).pipe(timeout(environment.apiTimeout));
  }

  getClientPagesCount(limit: number) {
    return this.http.get(`${environment.apiUrl}/clients/getClientPages/${limit}`).pipe(timeout(environment.apiTimeout));
  }

  getClients(page: number, limit: number): Observable<Client[]> {
    return this.http.get<Client[]>(`${environment.apiUrl}/clients/get/${page}/${limit}`).pipe(timeout(environment.apiTimeout));
  }

  getClient(clientNumber: number): Observable<Client> {
    return this.http.get<Client>(`${environment.apiUrl}/clients/getOne/${clientNumber}`).pipe(timeout(environment.apiTimeout));
  }

  updateClient(clientNumber: number, client: Client) {
    return this.http.post(`${environment.apiUrl}/clients/${clientNumber}`, client).pipe(timeout(environment.apiTimeout));
  }

  findClient(clientNumber: string): Observable<Client[]> {
    return this.http.get<Client[]>(`${environment.apiUrl}/clients/search/${clientNumber}`).pipe(timeout(environment.apiTimeout));
  }
}
