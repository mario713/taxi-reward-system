import { Injectable } from '@angular/core';
import { faRulerHorizontal } from '@fortawesome/free-solid-svg-icons';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  loading: boolean;
  error: boolean;
  error_msg: string;
  timeout: boolean;
  timeout_url: string;

  constructor() { }

  get state(): string {
    if(this.loading === true) {
      return 'loading';
    }
    
    if(this.loading === false && this.error === true && this.timeout === false) {
      return 'error';
    }

    if(this.loading === false && this.error === false && this.timeout === false) {
      return 'loaded';
    }

    if(this.loading === false && this.timeout === true) {
      return 'timeout';
    }

    return '';
  }

  startLoading() {
    this.error = false;
    this.timeout = false;
    this.loading = true;
  }

  finishLoading() {
    if(this.state === 'loading') {
      this.loading = false;
      this.error = false;
      this.timeout = false;
    }
  }

  throwTimeout(url: string) {
    this.timeout_url = url;
    this.timeout = true;
    this.loading = false;
  }

  throwError(err: any) {
    this.timeout = false;
    this.loading = false;
    this.error = true;
    this.error_msg = err;
  }
}
