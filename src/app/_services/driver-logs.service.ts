import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { DriverLog } from '../_models/DriverLog';

@Injectable({
  providedIn: 'root'
})
export class DriverLogsService {

  constructor(private http: HttpClient) { }

  getLogs(driverid: number, page: number, limit: number) {
    return this.http.get<DriverLog[]>(`${environment.apiUrl}/logs/driver/${driverid}/${page}/${limit}`)
  }

  getLogsPages(driverid: number, limit: number) {
    return this.http.get<Number>(`${environment.apiUrl}/logs/driverPages/${driverid}/${limit}`);
  }
}
