import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Course } from '../_models/Course';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private http: HttpClient) { }

  addCourse(course: Course) {
    return this.http.post(`${environment.apiUrl}/courses/add`, course);
  }

  getPagesCount(limit: number) {
    return this.http.get(`${environment.apiUrl}/courses/pages/${limit}`);
  }

  getCourses(page: number, limit: number): Observable<Course[]> {
    return this.http.get<Course[]>(`${environment.apiUrl}/courses/get/${page}/${limit}`);
  }

  getCourse(id: number): Observable<Course> {
    return this.http.get<Course>(`${environment.apiUrl}/courses/getOne/${id}`);
  }

  updateCourse(id: number, course: Course) {
    return this.http.post(`${environment.apiUrl}/courses/edit/${id}`, course);
  }

  deleteCourse(id: number) {
    return this.http.delete(`${environment.apiUrl}/courses/delete/${id}`);
  }

}
