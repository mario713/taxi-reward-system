import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../_models/User';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.apiUrl}/users`).pipe(timeout(environment.apiTimeout));
  }

  getUser(id: number): Observable<User> {
    return this.http.get<User>(`${environment.apiUrl}/users/${id}`).pipe(timeout(environment.apiTimeout));
  }

  addUser(user: User) {
    return this.http.post(`${environment.apiUrl}/users`, {username: user.username, email: user.email, password: user.password, rank: user.rank}).pipe(timeout(environment.apiTimeout));
  }

  updateUser(id: number, user: User) {
    let query: User = { username: user.username, email: user.email, rank: user.rank };
    if (user.password) { query.password = user.password; }

    return this.http.put(`${environment.apiUrl}/users/${id}`, query).pipe(timeout(environment.apiTimeout));
  }

  deleteUser(id: number) {
    return this.http.delete(`${environment.apiUrl}/users/${id}`).pipe(timeout(environment.apiTimeout));
  }
}
