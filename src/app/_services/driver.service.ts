import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Driver } from '../_models/Driver';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DriverService {

  constructor(private http: HttpClient) { }

  public getDrivers(): Observable<Driver[]> {
    return this.http.get<Driver[]>(`${environment.apiUrl}/drivers`);
  }

  public getDriver(id: number): Observable<Driver> {
    return this.http.get<Driver>(`${environment.apiUrl}/drivers/${id}`);
  }

  // tslint:disable-next-line: variable-name
  public addDriver(name: string, lastName: string, number: string, phoneNumber: string): Observable<Driver> {
    // tslint:disable-next-line: max-line-length
    return this.http.post<Driver>(`${environment.apiUrl}/drivers`, {name: name, lastName: lastName, number: number, phoneNumber: phoneNumber});
  }

  public deleteDriver(id: number): Observable<{}> {
    return this.http.delete(`${environment.apiUrl}/drivers/${id}`);
  }

  public updateDriver(id: number, name: string, lastName: string, number: number, phoneNumber: string) {
    // tslint:disable-next-line: max-line-length
    return this.http.put<Driver>(`${environment.apiUrl}/drivers/${id}`, {name: name, lastName: lastName, number: number, phoneNumber: phoneNumber});
  }
}
