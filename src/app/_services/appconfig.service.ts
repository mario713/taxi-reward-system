import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Settings } from '../_models/Settings';
import { environment } from 'src/environments/environment';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class AppconfigService {

  private configSettings: any = null

  constructor(private http: HttpClient,
              private translate: TranslateService) { }

  public get settings() {
    return this.configSettings;
  }

  public load(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<Settings>(`${environment.apiUrl}/settings`)
            .pipe(first())
            .subscribe(
              (response: any) => {
                this.configSettings = response;
                resolve(true);
              }, (error) => {
                console.error(`Critical Error: ${error}`);
                resolve(true);
              }
            );
    });
  }
}
