import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Settings } from '../_models/Settings';
import { environment } from 'src/environments/environment';
import { timeout, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  constructor(private http: HttpClient) { }

  getSettings(): Observable<Settings> {
    return this.http.get<Settings>(`${environment.apiUrl}/settings`);//.pipe(timeout(environment.apiTimeout));
  }

  updateSettings(settings: Settings) {
    return this.http.post(`${environment.apiUrl}/settings`, {title: settings.title, language: settings.language}).pipe(timeout(environment.apiTimeout));
  }
}
