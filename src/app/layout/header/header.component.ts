import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/_services/auth.service';
import { User } from 'src/app/_models/User';
import * as jwt_decode from 'jwt-decode';
import { Subject, Observable, of } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged, map, tap, switchMap, first, catchError } from 'rxjs/operators';
// tslint:disable-next-line: max-line-length
import { faHome, faCar, faIdCard, faUsers, faDatabase, faBars, faQuestionCircle, faCog, faPowerOff, faUserCircle, faChild } from '@fortawesome/free-solid-svg-icons';
import { NgbDropdownConfig, NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';
import { FaConfig } from '@fortawesome/angular-fontawesome';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ClientService } from 'src/app/_services/client.service';
import { Client } from 'src/app/_models/Client';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnDestroy {

  currentUser: User;
  private username: string;
  private userid: number;
  private destroy$: Subject<void> = new Subject<void>();
  searchForm: FormGroup;
  
  states = ['Alabama', 'Dupa', 'Kupa'];
  model: any;

  

  // Icons
  faSearch = faSearch;
  faHome = faHome;
  faCar = faCar;
  faIdCard = faIdCard;
  faUsers = faUsers;
  faDatabase = faDatabase;
  faBars = faBars;
  faQuestionCircle = faQuestionCircle;
  faCog = faCog;
  faPowerOff = faPowerOff;
  faUserCircle = faUserCircle;
  faChild = faChild;

  search = (text$: Observable<string>) => {
    return text$.pipe(
        debounceTime(400),
        distinctUntilChanged(),
        // switchMap allows returning an observable rather than maps array
        switchMap((searchText) =>  this.clientService.findClient(searchText))
    );
  }

  constructor(
    private router: Router,
    private elementRef: ElementRef,
    private authService: AuthService,
    private mobileMenu: NgbDropdownConfig,
    private config: FaConfig,
    private formBuilder: FormBuilder,
    private clientService: ClientService) {
    this.authService.currentUser.pipe(takeUntil(this.destroy$))
        .subscribe(user => {
          if (user != null) {
            this.userid = jwt_decode(user.token).id;
            this.username = jwt_decode(user.token).username;
          }
    });

    this.searchForm = this.formBuilder.group({
      clientNumber: ''
    });

    config.fixedWidth = true;
  }

  viewClient(id: string) {
    this.router.navigate([`/clients/view/${id}`]);
  }

  searchClient(event: NgbTypeaheadSelectItemEvent) {
    this.router.navigate([`/clients/view/${event.item}`]);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  logout() {
    // this.authService.logoff().pipe(first())
    //       .subscribe((response) => {
    //         console.log(JSON.stringify(response));
    //       }, (error) => {
    //         console.log(JSON.stringify(error));
    //       });
    // this.authService.logout();
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
