import { Component, OnInit } from '@angular/core';
import { faHome, faCar, faIdCard, faUsers, faDatabase, faBars, faQuestionCircle, faCog, faChild } from '@fortawesome/free-solid-svg-icons';
import { FaConfig } from '@fortawesome/angular-fontawesome';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.less']
})
export class SidebarComponent implements OnInit {

  faHome = faHome;
  faCar = faCar;
  faIdCard = faIdCard;
  faUsers = faUsers;
  faDatabase = faDatabase;
  faBars = faBars;
  faQuestionCircle = faQuestionCircle;
  faCog = faCog;
  faChild = faChild;

  constructor(private config: FaConfig,
              private translate: TranslateService) {
    config.fixedWidth = true;
  }

  ngOnInit() {
  }

  selectLanguage(lang: string) {
    console.log(`Changing lang: ${lang}`); //Debug - Delete later
    if(lang == 'en' || lang == 'pl') {
      this.translate.use(lang);
    }
  }

}
