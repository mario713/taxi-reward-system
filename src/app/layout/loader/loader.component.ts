import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/_services/loader.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.less']
})
export class LoaderComponent implements OnInit {

  constructor(private loader: LoaderService,
              private router: Router) { }

  ngOnInit(): void {
  }

  reload() {
    //console.log(`Going To: ${this.loader.timeout_url}`);
    this.router.navigateByUrl(this.loader.timeout_url);
    
  }

}
