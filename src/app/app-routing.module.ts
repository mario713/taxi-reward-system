import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HelpComponent } from './help/help.component';
import { LoginComponent } from './auth/login/login.component';
import { PassResetComponent } from './auth/pass-reset/pass-reset.component';
import { EmailPassComponent } from './auth/email-pass/email-pass.component';
import { AuthGuard } from './_helpers/auth.guard';
import { DriversListComponent } from './drivers/drivers-list/drivers-list.component';
import { AddDriverComponent } from './drivers/add-driver/add-driver.component';
import { EditDriverComponent } from './drivers/edit-driver/edit-driver.component';
import { ViewDriverComponent } from './drivers/view-driver/view-driver.component';
import { PermsListComponent } from './perms/perms-list/perms-list.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AddPermComponent } from './perms/add-perm/add-perm.component';
import { EditPermComponent } from './perms/edit-perm/edit-perm.component';
import { UsersListComponent } from './users/users-list/users-list.component';
import { AddUserComponent } from './users/add-user/add-user.component';
import { EditUserComponent } from './users/edit-user/edit-user.component';
import { ViewUserComponent } from './users/view-user/view-user.component';
import { AddCourseComponent } from './courses/add-course/add-course.component';
import { CoursesListComponent } from './courses/courses-list/courses-list.component';
import { EditCourseComponent } from './courses/edit-course/edit-course.component';
import { ViewClientComponent } from './clients/view-client/view-client.component';
import { EditClientComponent } from './clients/edit-client/edit-client.component';
import { SettingsComponent } from './settings/settings.component';
import { ServerErrorComponent } from './server-error/server-error.component';
import { SettingsResolveService } from './_resolvers/settings-resolve.service';
import { RanksResolveService } from './_resolvers/ranks-resolve.service';
import { RankResolveService } from './_resolvers/rank-resolve.service';
import { UsersResolveService } from './_resolvers/users-resolve.service';
import { UserResolveService } from './_resolvers/user-resolve.service';
import { ClientsListComponent } from './clients/clients-list/clients-list.component';
import { ClientsResolveService } from './_resolvers/clients-resolve.service';
import { ClientsPagesResolveService } from './_resolvers/clients-pages-resolve.service';
import { ClientResolveService } from './_resolvers/client-resolve.service';
import { DriversResolveService } from './_resolvers/drivers-resolve.service';
import { DriverResolveService } from './_resolvers/driver-resolve.service';
import { CourseResolveService } from './_resolvers/course-resolve.service';
import { CoursesResolveService } from './_resolvers/courses-resolve.service';
import { GratisResolverService } from './_resolvers/gratis-resolver.service';
import { LogsComponent } from './logs/logs.component';


const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: DashboardComponent, pathMatch: 'full' },
      { path: 'courses', component: CoursesListComponent, runGuardsAndResolvers: "always", resolve: { drivers: DriversResolveService, courses: CoursesResolveService, gratisClients: GratisResolverService } },
      { path: 'courses/add', component: AddCourseComponent, runGuardsAndResolvers: "always", resolve: { drivers: DriversResolveService } },
      { path: 'courses/edit/:id', component: EditCourseComponent, runGuardsAndResolvers: "always", resolve: { course: CourseResolveService, drivers: DriversResolveService } },
      { path: 'drivers', component: DriversListComponent, runGuardsAndResolvers: "always", resolve: { drivers: DriversResolveService } },
      { path: 'drivers/add', component: AddDriverComponent },
      { path: 'drivers/edit/:id', component: EditDriverComponent, runGuardsAndResolvers: "always", resolve: { driver: DriverResolveService } },
      { path: 'drivers/view/:id', component: ViewDriverComponent, runGuardsAndResolvers: "always", resolve: { driver: DriverResolveService } },
      { path: 'clients', component: ClientsListComponent, runGuardsAndResolvers: "always", resolve: { clients: ClientsResolveService, pages: ClientsPagesResolveService } },
      { path: 'clients/view/:id', component: ViewClientComponent, runGuardsAndResolvers: "always", resolve: { client: ClientResolveService } },
      { path: 'clients/edit/:id', component: EditClientComponent, runGuardsAndResolvers: "always", resolve: { client: ClientResolveService } },
      { path: 'users', component: UsersListComponent, runGuardsAndResolvers: "always", resolve: { users: UsersResolveService, ranks: RanksResolveService } },
      { path: 'users/add', component: AddUserComponent, runGuardsAndResolvers: "always", resolve: { ranks: RanksResolveService } },
      { path: 'users/edit/:id', component: EditUserComponent, runGuardsAndResolvers: "always", resolve: { user: UserResolveService, ranks: RanksResolveService } },
      { path: 'users/view/:id', component: ViewUserComponent, runGuardsAndResolvers: "always", resolve: { user: UserResolveService, ranks: RanksResolveService } },
      { path: 'perms', component: PermsListComponent, runGuardsAndResolvers: "always", resolve: { ranks: RanksResolveService } },
      { path: 'perms/add', component: AddPermComponent },
      { path: 'perms/edit/:id', component: EditPermComponent, runGuardsAndResolvers: "always", resolve: { rank: RankResolveService } },
      { path: 'settings', component: SettingsComponent, runGuardsAndResolvers: "always", resolve: { settings: SettingsResolveService } },
      { path: 'logs', component: LogsComponent },
      { path: 'help', component: HelpComponent }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'pass-reset/:id', component: PassResetComponent },
  { path: 'pass-reset', component: EmailPassComponent },
  { path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
