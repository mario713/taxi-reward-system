import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppconfigService } from '../_services/appconfig.service';

export function init_app(appConfigService: AppconfigService) {
  return () => appConfigService.load();
}

@NgModule({
  imports: [ HttpClientModule ],
  providers: [
    AppconfigService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppconfigService], multi: true }
  ]
})
export class AppconfigModule { }
