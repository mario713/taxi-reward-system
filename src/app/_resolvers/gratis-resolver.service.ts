import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Client } from '../_models/Client';
import { Observable, of } from 'rxjs';
import { ClientService } from '../_services/client.service';
import { LoaderService } from '../_services/loader.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GratisResolverService implements Resolve<Client[]> {

  constructor(private clientService: ClientService,
              private loader: LoaderService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Client[]> {
    return this.clientService.getGratisClients(0, 10)
      .pipe(catchError(err => {
        if(err.name) {
          if(err.name.match('TimeoutError')) {
            this.loader.throwTimeout(`/${route.url.join('/')}`);
            return of(new Array<Client>());
          }

          this.loader.throwError(err);
          return of(new Array<Client>());
        }
      }));
  }
}
