import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { ClientService } from '../_services/client.service';
import { LoaderService } from '../_services/loader.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientsPagesResolveService implements Resolve<Number | Object> {

  constructor(private clientService: ClientService,
              private loader: LoaderService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Number | Object> {
    return this.clientService.getClientPagesCount(10)
      .pipe(catchError(err => {
        if(err.name) {
          if(err.name.match('TimeoutError')) {
            this.loader.throwTimeout(`/${route.url.join('/')}`);
            return of(new Number());
          }
          
          this.loader.throwError(err);
          return of(new Number());
        }
      }))
    
  }
}
