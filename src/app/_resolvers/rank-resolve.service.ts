import { Injectable } from '@angular/core';
import { RankService } from '../_services/rank.service';
import { LoaderService } from '../_services/loader.service';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Rank } from '../_models/Rank';
import { Observable, EMPTY, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RankResolveService implements Resolve<Rank> {

  constructor(private rankService: RankService,
              private loader: LoaderService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Rank> {
  return this.rankService.getRank(route.params['id'])
    .pipe(catchError(err => {
      if(err.name) { 
        if(err.name.match('TimeoutError')) {
          this.loader.throwTimeout(`/${route.url.join('/')}`);
          return of(new Rank());
        }
      }

      this.loader.throwError(err);
      return of(new Rank());
    }));
  }
}
