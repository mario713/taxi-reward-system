import { Injectable } from '@angular/core';
import { Rank } from '../_models/Rank';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { LoaderService } from '../_services/loader.service';
import { RankService } from '../_services/rank.service';
import { Observable, of } from 'rxjs';
import { catchError, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RanksResolveService implements Resolve<Rank[]> {

  constructor(private rankService: RankService,
              private loader: LoaderService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Rank[]> {
    return this.rankService.getRanks()
      .pipe(catchError(err => {
        if(err.name) { 
          if(err.name.match('TimeoutError')) {
            this.loader.throwTimeout(`/${route.url.join('/')}`);
            return of(new Array<Rank>());
          }
        }
        
        this.loader.throwError(err);
        return of(new Array<Rank>());
      }));
  }
}
