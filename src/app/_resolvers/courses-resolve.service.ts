import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Course } from '../_models/Course';
import { CourseService } from '../_services/course.service';
import { LoaderService } from '../_services/loader.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CoursesResolveService implements Resolve<Course[]> {

  constructor(private courseService: CourseService,
              private loader: LoaderService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Course[]> {
    return this.courseService.getCourses(0, 10)
      .pipe(catchError(err => {
        if(err.name) {
          if(err.name.match('TimeoutError')) {
            this.loader.throwTimeout(`/${route.url.join('/')}`);
            return of(new Array<Course>());
          }

          this.loader.throwError(err);
          return of(new Array<Course>());
        }
      }));
  }
}
