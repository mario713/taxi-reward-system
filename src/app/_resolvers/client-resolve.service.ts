import { Injectable } from '@angular/core';
import { Client } from '../_models/Client';
import { ClientService } from '../_services/client.service';
import { Observable, of } from 'rxjs';
import { LoaderService } from '../_services/loader.service';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClientResolveService implements Resolve<Client> {

  constructor(private clientService: ClientService,
              private loader: LoaderService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Client> {
    return this.clientService.getClient(route.params['id'])
      .pipe(catchError(err => {
        if(err.name) {
          if(err.name.match('TimeoutError')) {
            this.loader.throwTimeout(`/${route.url.join('/')}`);
            return of(new Client());
          }

          this.loader.throwError(err);
          return of(new Client());
        }
      }))
  }
}
