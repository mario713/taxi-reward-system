import { Injectable } from '@angular/core';
import { Driver } from '../_models/Driver';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { DriverService } from '../_services/driver.service';
import { LoaderService } from '../_services/loader.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DriverResolveService implements Resolve<Driver> {

  constructor(private driverService: DriverService,
              private loader: LoaderService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Driver> {
    return this.driverService.getDriver(route.params['id'])
      .pipe(catchError(err => {
        if(err.name) {
          if(err.name.match('TimeoutError')) {
            this.loader.throwTimeout(`/${route.url.join('/')}`);
            return of(new Driver());
          }
        }

        this.loader.throwError(err);
        return of(new Driver())
      }));
  }
}
