import { Injectable } from '@angular/core';
import { User } from '../_models/User';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { LoaderService } from '../_services/loader.service';
import { UserService } from '../_services/user.service';
import { Observable, of } from 'rxjs';
import { catchError, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersResolveService implements Resolve<User[]> {

  constructor(private userService: UserService,
              private loader: LoaderService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<User[]> {
    return this.userService.getUsers()
      .pipe(catchError(err => {
        if(err.name) {
          if(err.name.match('TimeoutError')) {
            this.loader.throwTimeout(`/${route.url.join('/')}`);
            return of(new Array<User>());
          }
        }

        this.loader.throwError(err);
        return of(new Array<User>());
      }));
  }
}
