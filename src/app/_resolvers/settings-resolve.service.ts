import { Injectable } from '@angular/core';
import { Settings } from '../_models/Settings';
import { Resolve, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { SettingsService } from '../_services/settings.service';
import { first, catchError } from 'rxjs/operators';
import { Observable, throwError, EMPTY } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { LoaderService } from '../_services/loader.service';

@Injectable({
  providedIn: 'root'
})
export class SettingsResolveService implements Resolve<Settings> {

  constructor(private settingsService: SettingsService,
              private loader: LoaderService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Settings> {
    return this.settingsService.getSettings()
          .pipe(catchError(err => {
            if(err.name) {
              if(err.name.match('TimeoutError')) {
                this.loader.throwTimeout(`/${route.url.join('/')}`);
                return EMPTY;
              }
            }

            this.loader.throwError(err);
            return EMPTY;
          }));
  }
}
