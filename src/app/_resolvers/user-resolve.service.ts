import { Injectable } from '@angular/core';
import { Resolve, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { User } from '../_models/User';
import { Observable, EMPTY } from 'rxjs';
import { UserService } from '../_services/user.service';
import { LoaderService } from '../_services/loader.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserResolveService implements Resolve<User> {

  constructor(private userService: UserService,
              private loader: LoaderService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<User> {
    return this.userService.getUser(route.params['id'])
      .pipe(catchError(err => {
        if(err.name) {
          if(err.name.match('TimeoutError')) {
            this.loader.throwTimeout(`/${route.url.join('/')}`);
            return EMPTY;
          }
        }

        this.loader.throwError(err);
        return EMPTY;
      }));
  }
}
