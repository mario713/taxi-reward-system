import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Rank } from '../_models/Rank';
import { Observable, throwError } from 'rxjs';
import { RankService } from '../_services/rank.service';
import { HttpErrorResponse } from '@angular/common/http';
import { LoaderService } from '../_services/loader.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PermsResolveService implements Resolve<Rank[]> {

  constructor(private rankService: RankService,
              private loader: LoaderService) { }

  resolve(): Observable<Rank[]> {
    return this.rankService.getRanks()
          .pipe(catchError((err: HttpErrorResponse) => {
            this.loader.throwError(err);
            return throwError(err);
          }));
  }
}
