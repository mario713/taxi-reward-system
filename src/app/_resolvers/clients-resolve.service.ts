import { Injectable } from '@angular/core';
import { Client } from '../_models/Client';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { ClientService } from '../_services/client.service';
import { LoaderService } from '../_services/loader.service';
import { catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientsResolveService implements Resolve<Client[]> {

  constructor(private clientsService: ClientService,
              private loader: LoaderService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Client[]> {
    return this.clientsService.getClients(0, 10)
      .pipe(catchError(err => {
        if(err.name) {
          if(err.name.match('TimeoutError')) {
            this.loader.throwTimeout(`/${route.url.join('/')}`);
            return of(new Array<Client>());
          }

          this.loader.throwError(err);
          return of(new Array<Client>());
        }
      }));
  }
}
